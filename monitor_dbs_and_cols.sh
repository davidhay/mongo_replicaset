#!/bin/sh
. ./monitor_mongoEval.sh
MSCRIPT=$(cat <<'EOF'
db.getMongo().getDBNames().forEach(dbName => {
   print("DB ["+dbName+"]");
   const other = db.getSiblingDB(dbName);
   other.getCollectionNames().forEach(col => {
    print("DB ["+dbName+"] collection["+col+"]");
  });
  print("---------------");
});
EOF
)
mongoEval "$MSCRIPT"
