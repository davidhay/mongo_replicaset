USER=muser
PASSWD=mpass
HOST1=mongo1.hee.com:27011
HOST2=mongo2.hee.com:27012
HOST3=mongo3.hee.com:27013

SCRIPT='print("@["+db.getMongo()+"]primary is ["+db.isMaster().primary+"]")';

function mEval(){
  HOST=$1
  DBOPTS=$2
  mongo --quiet "mongodb://$USER:$PASSWD@$HOST/$DBOPTS" --eval "$SCRIPT"
}
function mongoEval(){
  HOST=$1
  mEval "$HOST" "admin"
}
function mongoEvalWithRs() {
  HOST=$1
  mEval "$HOST" "admin?replicaSet=rs0" | grep -v NETWORK
}
function forEachHost(){
  CMD=$1
  for mongoHost in "$HOST1" "$HOST2" "$HOST3"
  do
      $CMD $mongoHost
  done
}

forEachHost mongoEval

forEachHost mongoEvalWithRs
