#!/bin/sh

VOL1=volMongo1
VOL2=volMongo2
VOL3=volMongo3

function YorN(){
  local resultYorN="undef"
  CODE=$1
  case "$CODE" in
   0) resultYorN="Y" ;;
   *) resultYorN="N" ;;
  esac
  echo $resultYorN
}

function volumeExists(){
  NAME=$1
  docker volume inspect "$NAME" > /dev/null 2>&1
  CODE=$?
  RESULT=$(YorN "$CODE")
  echo $RESULT
}

function createVolumeIfReqd() {
  NAME=$1
  RESULT=$(volumeExists $NAME)
  if [ "$RESULT" == "Y" ]; then
    echo "volume [$NAME] exists"
  else
    echo "volume [$NAME] does not exist"
    docker volume create $NAME
  fi
}

for vol in $VOL1 $VOL2 $VOL3 $VOL4; do
    createVolumeIfReqd $vol
done
