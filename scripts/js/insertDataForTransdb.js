/*
 Checks that:
 1) The 'txuser' can authenticate against the 'admin' db.
 2) The 'txuser' can insert data into the 'transdb' db
*/
print("START : insertDataForTransdb.js");
//we have to make sure we're connecting to admin db to authenticate.
admin = db.getSiblingDB('admin');
admin.auth('txuser','txpass');
//switch to transdb db
db = db.getSiblingDB('transdb');

const CURRENT_USER = db.runCommand({connectionStatus : 1}).authInfo.authenticatedUsers[0].user;
const CURRENT_DB = db.getName();
print("CURRENT USER["+CURRENT_USER+"]DB["+CURRENT_DB+"]");

db.test.insert([
  {'name':'Tom'},
  {'name':'Dick'},
  {'name':'Harry'}
]);

var testData = db.test.find().toArray();
printjson(testData);
print("END : insertDataForTransdb.js");
