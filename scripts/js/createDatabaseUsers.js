/*
 Creates a separate (non admin) user for each mongo database.
*/
function createDatabaseUser(database, user, pass) {
  db.createUser({
      user: user,
      pwd: pass,
      roles: [ {
        role: 'readWrite',
        db: database
      }]
  });
}

print("START : createDatabaseUsers.js");

DB_USERS_CONFIG.forEach( dbUserConfig => {
  const [db, user, pass] = dbUserConfig.split("|")
  createDatabaseUser(db, user, pass);
});

print("END : createDatabaseUsers.js");
