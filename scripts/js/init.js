print("START : init.js");

//HOSTS - loaded from /tmp/HOSTS.js
if(!HOSTS || HOSTS.length === 0){
  throw new Error('HOSTS is undefined or empty');
}
print("----- HOSTS -----");
printjson(HOSTS);

//DB_USERS_CONFIG - loaded from /run/secrets/DB_USERS_CONFIG.js
if(!DB_USERS_CONFIG || DB_USERS_CONFIG.length === 0){
  throw new Error('DB_USERS_CONFIG is undefined or empty');
}
print("----- DB_USERS_CONFIG ----- ");
printjson(DB_USERS_CONFIG);

//INITIALISE REPLICA SET
load('initialiseReplicaSet.js');

if(db.isMaster().ismaster) {
  //create database users
  load('createDatabaseUsers.js');

  //load some test data into database
  load('insertDataForTransdb.js');
} else {
  /*
  we can't Create Users or insert data if the Replica Set has not
  been initialised!!
  */
  print('FAILED TO INITIAISE REPLICA SET : DID NOT CREATE USERS OR INSERT TEST DATA');
}
print("END : init.js");
quit();
