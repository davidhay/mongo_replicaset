/*
 This initialises the Mongo Cluster into a ReplicaSet 'rs0'
 AND waits until the replicaset has initialised before returning (limit is 60 seconds)
*/
print("START : initialiseReplicaSet.js")
const MEMBERS = HOSTS.map( (host,idx) => ({_id:idx, host: host}));
const LIMIT=60;
const ONE_SECOND_IN_MS=1000;
var count=0;
const t1 = new Date();

//initialse the replica set
rs.initiate({
  _id: 'rs0',
  members: MEMBERS
});

//wait until initialised
while(count < LIMIT && !(db.isMaster().ismaster)){
  count++;
  sleep(ONE_SECOND_IN_MS);
};

//output the results
const t2 = new Date();
const diffMs=(t2-t1);
printjson(db.isMaster());
if(db.isMaster().ismaster){
  print("ReplicaSet Initialised After ["+count+"]secs.");
  print("ReplicaSet Initialised After ["+diffMs+"]ms.");
} else {
  print("Failed to Initialised After ["+count+"]secs.");
}
print("END : initialiseReplicaSet.js")
