
#!/bin/bash
#
# THIS SCRIPT RUNS INSIDE THE DOCKER CONTAINER ON THE FIRST MONGO NODE ONLY
# AND ONLY WHEN CREATING THE CONTAINERS in the docker-compose file.
#
echo "IN================================================================init.sh"

cat <<EOF >> '/tmp/HOSTS.js'
const HOSTS=[];
HOSTS.push("$HOST_1");
HOSTS.push("$HOST_2");
HOSTS.push("$HOST_3");
EOF

initialiseMongo() {
 JS_DIR='/docker-entrypoint-initdb.d/js'
 # the mongo working directory is '/'' - we have to 'cd' to 'load' the js files
 COMMAND="cd('$JS_DIR');load('/tmp/HOSTS.js');load('/run/secrets/DB_USERS_CONFIG.js');;load('init.js');"
 #echo "COMMAND is [$COMMAND]"
 mongo "mongodb://$MONGO_INITDB_ROOT_USERNAME:$MONGO_INITDB_ROOT_PASSWORD@$HOST_1/admin" --eval "$COMMAND"
 echo "OUT##############################################################init.sh"
}
# we have to wait a few seconds otherwise we can't connect to mongo :-(
(sleep 5 && initialiseMongo )&
