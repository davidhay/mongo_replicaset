#!/bin/sh
#
# This script is imported into others to run commands against MongoDB
#
function mongoEval(){
  SCRIPT=$1
  mongo --quiet 'mongodb://muser:mpass@mongo1.hee.com:27011/admin?replicaSet=rs0' --eval "$SCRIPT" | grep -v NETWORK
}
